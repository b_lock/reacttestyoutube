import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {search} from "../actions/index";
import Search from "../components/Search";
import ListItem from "../components/ListItem";

import '../style/index.css';

class App extends Component {
  static propTypes = {
    search: PropTypes.func.isRequired,
    videos: PropTypes.array,
    // Injected by React Router
    children: PropTypes.node
  };

  handleChange = nextValue => {
    this.props.search(nextValue);
  };

  render() {
    const { children } = this.props;
    return (
      <div className="container">
        <br/>
        <div className="row">
          <div className="col-md-8">
            <Search
              onChange={this.handleChange}/>
              <hr/>
            {children}
          </div>
          <div className=" col-md-4 right-bar">
            <div className="list-container">
              {this.props.videos.map((video) => {
                return (
                  <ListItem key={video.id.videoId} video={video}/>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    )
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    search: (text) => dispatch(search(text)),
  };
};
const mapStateToProps = (state) => {
  return {
    videos: state.videos
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App)
