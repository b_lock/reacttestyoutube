import {routerReducer as routing} from 'react-router-redux'
import {combineReducers} from 'redux'


const videos = (state = [], action) => {
  switch (action.type) {
    case "ADD_VIDEOS":
      return action.videos;
    default:
      return state;
  }
};

const rootReducer = combineReducers({
  videos,
  routing
});

export default rootReducer
