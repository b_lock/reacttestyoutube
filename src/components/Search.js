import React, {Component} from 'react'
import PropTypes from 'prop-types'


export default class Search extends Component {
  static propTypes = {
    onChange: PropTypes.func.isRequired
  };

  getInputValue = () => {
    return this.refs.input.value
  };

  handleGoClick = () => {
    this.props.onChange(this.getInputValue())
  };

  render() {
    return (
      <div className="input-group">
        <input className="form-control"
               ref="input"
               onKeyUp={this.handleGoClick}
               defaultValue={this.props.value}/>
        <span className="input-group-btn">
          <button className="btn btn-default"
                  onClick={this.handleGoClick}>
            Search!
          </button>
        </span>
      </div>
    )
  }
}
