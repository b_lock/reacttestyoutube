
const fetchSEarch = videos => ({
  type: "ADD_VIDEOS",
  videos
});

export const search = (searchText) => (dispatch) => {
  fetch(`https://www.googleapis.com/youtube/v3/search?key=AIzaSyDkqTVbdJD-voUvr1JopHnkW7IajfiOMuk&part=snippet&type=video&maxResults=10&q=${searchText}`, { method: 'GET' }).then(response => {
    if (response.status >= 400) {
      throw new Error("Bad response from server");
    }
    return response.json();
  })
    .then(json => {
      const videos = json.items;
      return dispatch(fetchSEarch(videos));
    });
};
