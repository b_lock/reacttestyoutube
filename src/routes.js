import React from 'react'
import {Route} from 'react-router'
import App from './containers/App'
import Video from "./components/Video";

export default <Route path="/" component={App}>
  <Route path="/movie/:video"
         component={Video}/>
</Route>
