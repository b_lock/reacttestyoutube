import React from 'react';

const Video = (props) => {
  const { video } = props.params;
  const videoUrl = `http://www.youtube.com/embed/${video}/?autoplay=1`;
  return (
    <div className="video-item">
      <iframe id="player"
              title="video"
              type="text/html"
              src={videoUrl}/>
    </div>
  )
};

export default Video
