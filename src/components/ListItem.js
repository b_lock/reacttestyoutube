import React from 'react'
import PropTypes from 'prop-types'
import {Link} from 'react-router'

const ListItem = (props) => {
  const { video } = props;
  return (
    <div className="list-item">
      <Link to={`/movie/${video.id.videoId}`}>
        <img src={video.snippet.thumbnails.medium.url} alt=""/><br/>
        {video.snippet.title}
      </Link>
    </div>
  )
};

ListItem.propTypes = {
  video: PropTypes.object.isRequired,
};

export default ListItem
